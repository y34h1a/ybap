package com.y34h1a.youtubeaudioplayer.interfaces;

import com.y34h1a.youtubeaudioplayer.pojo.YouTubeVideo;

import java.util.ArrayList;

/**
 * Created by y34h1a on 8/29/16.
 */
public interface YoutubeVideosReceiver {
    void onVideosReceiver(ArrayList<YouTubeVideo> youTubeVideos);
}
