package com.y34h1a.youtubeaudioplayer.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

/**
 * Created by y34h1a on 8/28/16.
 */
public class NetworkConf {
    private Activity activity;

    public NetworkConf(Activity activity) {
        this.activity = activity;
    }

    /*
    * Checking internet connection available or not
    * */

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void createNetErrorDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Network Conneciton Not Available")
                .setTitle("Connection Error")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                                activity.startActivity(intent);
                            }
                        }
                )
                .setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }
                );
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
