package com.y34h1a.youtubeaudioplayer.utils;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.y34h1a.youtubeaudioplayer.constant.Constants;
import com.y34h1a.youtubeaudioplayer.interfaces.YoutubeVideosReceiver;
import com.y34h1a.youtubeaudioplayer.pojo.YouTubeVideo;
import java.io.IOException;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by y34h1a on 8/29/16.
 */
public class YouTubeSearch {
    private static final String TAG = "YOUTUBE SEARCH CLASS";
    private String appName;

    private Handler handler;
    private Activity activity;

    private YouTube youtube;

    private Fragment playlistFragment;

    private String mChosenAccountName;

    private YoutubeVideosReceiver youtubeVideosReceiver;

    private static final int REQUEST_AUTHORIZATION = 3;

    public YouTubeSearch(Activity activity, Fragment playlistFragment) {
        this.activity = activity;
        this.playlistFragment = playlistFragment;
        handler = new Handler();
        appName = "YoutubeAudioPlayer";
    }

    public void setYoutubeVideosReceiver(YoutubeVideosReceiver youtubeVideosReceiver) {
        this.youtubeVideosReceiver  = youtubeVideosReceiver;
    }

    public void searchVideos(final String keywords){
        new Thread(){
            @Override
            public void run() {
                try{

                    // This object is used to make YouTube Data API requests.
                    youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
                        @Override
                        public void initialize(HttpRequest httpRequest) throws IOException {

                        }
                    }).setApplicationName(appName).build();

                    YouTube.Search.List searchList;
                    YouTube.Videos.List videosList;

                    searchList = youtube.search().list("id,snippet");
                    searchList.setKey(Constants.YOUTUBE_API_KEY);
                    searchList.setType("video");
                    searchList.setMaxResults(Constants.NUMBER_OF_VIDEOS_RETURNED);
                    searchList.setFields("items(id/videoId,snippet/title,snippet/thumbnails/high/url,snippet/channelTitle)");

                    videosList = youtube.videos().list("id,contentDetails,statistics");
                    videosList.setKey(Constants.YOUTUBE_API_KEY);
                    videosList.setFields("items(contentDetails/duration,statistics/viewCount)");

                    searchList.setQ(keywords);
                    SearchListResponse searchListResponse = searchList.execute();
                    List<SearchResult> searchResults = searchListResponse.getItems();

                    StringBuilder contentDetails = new StringBuilder();

                    int ii = 0;
                    for(SearchResult result : searchResults) {
                        contentDetails.append(result.getId().getVideoId());
                        if(ii < 49)
                            contentDetails.append(",");
                        ii++;
                    }

                    //find video list
                    videosList.setId(contentDetails.toString());
                    VideoListResponse response = videosList.execute();
                    List<Video> videoResults = response.getItems();

                    //make items for displaying in listview
                    ArrayList<YouTubeVideo> items = new ArrayList<YouTubeVideo>();
                    for(int i = 0; i < searchResults.size(); i++) {
                        YouTubeVideo item = new YouTubeVideo();
                        //searchList list info
                        item.setTitle(searchResults.get(i).getSnippet().getTitle());
                        item.setThumbnailURL(searchResults.get(i).getSnippet().getThumbnails().getHigh().getUrl());
                        item.setChannelTitle(searchResults.get(i).getSnippet().getChannelTitle());
                        item.setId(searchResults.get(i).getId().getVideoId());

                        //video info
                        if(videoResults.get(i) != null){
                            BigInteger viewsNumber = videoResults.get(i).getStatistics().getViewCount();
                            String viewsFormatted = NumberFormat.getIntegerInstance().format(viewsNumber);
                            item.setViewCount(viewsFormatted);
                            String isoTime = videoResults.get(i).getContentDetails().getDuration();
                            String time = Utils.convertISO8601DurationToNormalTime(isoTime);
                            item.setDuration(time);
                        } else {
                            item.setDuration("NA");
                        }

                        //add to the list
                        items.add(item);
                    }
                    youtubeVideosReceiver.onVideosReceiver(items);

                }catch (IOException e) {
                    Log.e(TAG, "Could not initialize: " + e);
                    e.printStackTrace();
                    return;
                }
            }
        }.start();
    }




}
