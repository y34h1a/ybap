package com.y34h1a.youtubeaudioplayer.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.UndoAdapter;
import com.nhaarman.listviewanimations.util.Swappable;
import com.squareup.picasso.Picasso;
import com.y34h1a.youtubeaudioplayer.R;
import com.y34h1a.youtubeaudioplayer.database.YouTubeSqlDb;
import com.y34h1a.youtubeaudioplayer.pojo.YouTubeVideo;

import java.util.List;

/**
 * Created by y34h1a on 8/29/16.
 */
public class VideosAdapter extends ArrayAdapter<YouTubeVideo> implements Swappable, UndoAdapter {
    private Activity context;
    private final List<YouTubeVideo> list;
    private boolean[] itemChecked;
    private boolean isFavoriteList;

    public VideosAdapter(Activity context, List<YouTubeVideo> list, boolean isFavoriteList){
        super(context, R.layout.video_item, list);
        this.list = list;
        this.context = context;
        this.itemChecked = new boolean[50];
        this.isFavoriteList = isFavoriteList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = context.getLayoutInflater().inflate(R.layout.video_item,parent,false);
        }
        ImageView thumbnail = (ImageView) convertView.findViewById(R.id.video_thumbnail);
        TextView title = (TextView) convertView.findViewById(R.id.video_title);
        TextView duration = (TextView) convertView.findViewById(R.id.video_duration);
        TextView viewCount = (TextView) convertView.findViewById(R.id.views_number);
        CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.imageButton);
        TextView channelTitle = (TextView) convertView.findViewById(R.id.channelTitle);

        final YouTubeVideo searchResult = list.get(position);

        Picasso.with(context).load(searchResult.getThumbnailURL()).into(thumbnail);
        title.setText(searchResult.getTitle());
        duration.setText(searchResult.getDuration());
        String viewcount = searchResult.getViewCount() + " views";
        viewCount.setText(viewcount);
        channelTitle.setText(searchResult.getChannelTitle());

        //set checked if exists in database
        if(YouTubeSqlDb.getInstance().videos(YouTubeSqlDb.VIDEOS_TYPE.FAVORITE).checkIfExists(searchResult.getId())){
            itemChecked[position] = true;
        }else{
            itemChecked[position] = false;
        }

        checkBox.setChecked(itemChecked[position]);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                itemChecked[position] = isChecked;
            }
        });

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((CheckBox) view).isChecked()){
                    YouTubeSqlDb.getInstance().videos(YouTubeSqlDb.VIDEOS_TYPE.FAVORITE).create(searchResult);
                } else {
                    YouTubeSqlDb.getInstance().videos(YouTubeSqlDb.VIDEOS_TYPE.FAVORITE).delete(searchResult.getId());
                    if(isFavoriteList){
                        list.remove(position);
                        notifyDataSetChanged();
                    }
                }
            }
        });

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void swapItems(int i, int i1) {
        YouTubeVideo firstItem = getItem(i);

        list.set(i, getItem(i1));
        list.set(i1, firstItem);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getUndoView(int i, @Nullable View convertView, @NonNull ViewGroup viewGroup) {
        View view = convertView;
        if(view == null){
            view  = LayoutInflater.from(getContext()).inflate(R.layout.undo_row, viewGroup, false);
        }
        return view;
    }

    @NonNull
    @Override
    public View getUndoClickView(@NonNull View view) {
        return view.findViewById(R.id.undo_row_undobutton);
    }
}
