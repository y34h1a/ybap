package com.y34h1a.youtubeaudioplayer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.y34h1a.youtubeaudioplayer.R;
import com.y34h1a.youtubeaudioplayer.adapter.VideosAdapter;
import com.y34h1a.youtubeaudioplayer.database.YouTubeSqlDb;
import com.y34h1a.youtubeaudioplayer.pojo.YouTubeVideo;
import com.y34h1a.youtubeaudioplayer.utils.BackgroundAudioService;
import com.y34h1a.youtubeaudioplayer.constant.Constants;
import com.y34h1a.youtubeaudioplayer.utils.NetworkConf;
import java.util.ArrayList;


public class FavourateFragment extends Fragment {
    private static final String TAG = "SMEDIC Favorites";
    private ArrayList<YouTubeVideo> favouriteVideos;

    private DynamicListView favoritesListView;
    private VideosAdapter videosAdapter;
    private NetworkConf conf;


    public FavourateFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        favouriteVideos = new ArrayList<>();
        conf = new NetworkConf(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_favourate, container, false);
        favoritesListView = (DynamicListView) v.findViewById(R.id.favourite_list);
        setupListViewAdapter();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        favouriteVideos.clear();
        favouriteVideos.addAll(YouTubeSqlDb.getInstance().videos(YouTubeSqlDb.VIDEOS_TYPE.FAVORITE).readAll());
        videosAdapter.notifyDataSetChanged();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && isResumed()){
            onResume();
        }
    }

    private void setupListViewAdapter() {
        videosAdapter = new VideosAdapter(getActivity(), favouriteVideos, true);
        SwingBottomInAnimationAdapter animationAdapter = new SwingBottomInAnimationAdapter(videosAdapter);
        favoritesListView.setAdapter(videosAdapter);

        addListeners();
    }

    private void addListeners() {
        favoritesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long id) {
                if(conf.isNetworkAvailable()) {
                    Toast.makeText(getActivity(), "Playing: " + favouriteVideos.get(pos).getTitle(), Toast.LENGTH_SHORT).show();
                    YouTubeSqlDb.getInstance().videos(YouTubeSqlDb.VIDEOS_TYPE.FAVORITE).create(favouriteVideos.get(pos));
                    Intent serviceIntent = new Intent(getActivity(), BackgroundAudioService.class);
                    serviceIntent.setAction(BackgroundAudioService.ACTION_PLAY);
                    serviceIntent.putExtra(Constants.YOUTUBE_TYPE, Constants.YOUTUBE_MEDIA_TYPE_PLAYLIST);
                    serviceIntent.putExtra(Constants.YOUTUBE_TYPE_PLAYLIST,favouriteVideos);
                    serviceIntent.putExtra(Constants.YOUTUBE_TYPE_PLAYLIST_VIDEO_POS, pos);
                    getActivity().startService(serviceIntent);
                }else {
                    conf.createNetErrorDialog();
                }
            }
        });
    }
}
