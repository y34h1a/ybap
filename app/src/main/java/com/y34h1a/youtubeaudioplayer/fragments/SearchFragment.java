package com.y34h1a.youtubeaudioplayer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.y34h1a.youtubeaudioplayer.interfaces.YoutubeVideosReceiver;
import com.y34h1a.youtubeaudioplayer.utils.BackgroundAudioService;
import com.y34h1a.youtubeaudioplayer.constant.Constants;
import com.y34h1a.youtubeaudioplayer.utils.NetworkConf;
import com.y34h1a.youtubeaudioplayer.R;
import com.y34h1a.youtubeaudioplayer.adapter.VideosAdapter;
import com.y34h1a.youtubeaudioplayer.pojo.YouTubeVideo;
import com.y34h1a.youtubeaudioplayer.utils.YouTubeSearch;

import java.util.ArrayList;

public class SearchFragment extends ListFragment implements YoutubeVideosReceiver {

    private static final String TAG = "Search Fragment";
    private Handler handler;
    private ArrayList<YouTubeVideo> searchResultsList;
    private ArrayList<YouTubeVideo> scrollResultsList;
    private VideosAdapter videoListAdapter;
    private ProgressBar loadingProgressBar;
    private NetworkConf networkConf;
    private YouTubeSearch youTubeSearch;
    private DynamicListView videosFoundListView;

    private int onScrollIndex = 0;
    private int mPrevTotalItemCount = 0;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        searchResultsList = new ArrayList<>();
        scrollResultsList = new ArrayList<>();
        networkConf = new NetworkConf(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container,false);
        loadingProgressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        return v;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if(isVisibleToUser && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        videosFoundListView = (DynamicListView) getListView();
        setupListViewAdapter();
        addListeners();

    }

    private void addListeners() {
        videosFoundListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                if(!networkConf.isNetworkAvailable()){
                    networkConf.createNetErrorDialog();
                    return;
                }

                Toast.makeText(getActivity(), "Playing: " + searchResultsList.get(pos).getTitle(), Toast.LENGTH_LONG).show();

                Intent serviceIntent = new Intent(getActivity(), BackgroundAudioService.class);
                serviceIntent.setAction(BackgroundAudioService.ACTION_PLAY);
                serviceIntent.putExtra(Constants.YOUTUBE_TYPE, Constants.YOUTUBE_MEDIA_TYPE_VIDEO);
                serviceIntent.putExtra(Constants.YOUTUBE_TYPE_VIDEO, searchResultsList.get(pos));
                getActivity().startService(serviceIntent);
            }
        });

        videosFoundListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(totalItemCount < Constants.NUMBER_OF_VIDEOS_RETURNED){
                    if(view.getAdapter() != null && ((firstVisibleItem + visibleItemCount) >= totalItemCount) && totalItemCount != mPrevTotalItemCount){
                        mPrevTotalItemCount = totalItemCount;
                        addMoreData();
                    }
                }
            }
        });
    }

    private void setupListViewAdapter() {
        videoListAdapter = new VideosAdapter(getActivity(), searchResultsList, false);
        SwingBottomInAnimationAdapter animationAdapter = new SwingBottomInAnimationAdapter(videoListAdapter);
        animationAdapter.setAbsListView(videosFoundListView);
        videosFoundListView.setAdapter(animationAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!getUserVisibleHint()){

        }
        youTubeSearch = new YouTubeSearch(getActivity(),this);
        youTubeSearch.setYoutubeVideosReceiver(this);
    }

    @Override
    public void onVideosReceiver(ArrayList<YouTubeVideo> youTubeVideos) {
        searchResultsList.clear();
        scrollResultsList.clear();
        scrollResultsList.addAll(youTubeVideos);
        handler.post(new Runnable() {
            @Override
            public void run() {
                loadingProgressBar.setVisibility(View.INVISIBLE);
            }
        });
        addMoreData();
    }

    private void addMoreData() {
        try{
            searchResultsList.addAll(scrollResultsList.subList(10 * onScrollIndex, onScrollIndex * 10 + 10));
        }catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
        onScrollIndex++;
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(videoListAdapter != null){
                    videoListAdapter.notifyDataSetChanged();
                }
            }
        });

        Log.i(TAG,"add more data called");
    }

    public void searchQuery(String query) {
        if (!networkConf.isNetworkAvailable()) {
            networkConf.createNetErrorDialog();
            return;
        }
        loadingProgressBar.setVisibility(View.VISIBLE);
        onScrollIndex = 0;
        youTubeSearch.searchVideos(query);
    }

}
