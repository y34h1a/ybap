/*
 * Copyright (C) 2016 SMedic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.y34h1a.youtubeaudioplayer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.y34h1a.youtubeaudioplayer.pojo.YouTubeVideo;

import java.util.ArrayList;

/**
 * SQLite database for storing recentlyWatchedVideos and playlist
 * Created by Stevan Medic on 17.3.16..
 */
public class YouTubeSqlDb {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "YouTubeDb.db";

    public static final String FAVORITES_TABLE_NAME = "favorites_videos";
    private static final String TAG = "SMEDIC TABLE SQL";

    public enum VIDEOS_TYPE {FAVORITE}

    private YouTubeDbHelper dbHelper;

    private Videos favoriteVideos;

    private static YouTubeSqlDb ourInstance = new YouTubeSqlDb();

    public static YouTubeSqlDb getInstance() {
        return ourInstance;
    }

    private YouTubeSqlDb() {
    }

    public void init(Context context){
        dbHelper = new YouTubeDbHelper(context);
        dbHelper.getWritableDatabase();

        favoriteVideos = new Videos(FAVORITES_TABLE_NAME);
    }

    public Videos videos(VIDEOS_TYPE type){
        if(type == VIDEOS_TYPE.FAVORITE) {
            return favoriteVideos;
        }
        return null;
    }

    public class Videos {
        private String tableName;

        private Videos(String tableName) {
            this.tableName = tableName;
        }

        public boolean create(YouTubeVideo video) {
            if(checkIfExists(video.getId())){
                return false;
            }

            //gets data repository in write mode
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            //create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();
            values.put(YouTubeVideoEntry.COLUMN_VIDEO_ID,video.getId());
            values.put(YouTubeVideoEntry.COLUMN_TITLE, video.getTitle());
            values.put(YouTubeVideoEntry.COLUMN_DURATION, video.getDuration());
            values.put(YouTubeVideoEntry.COLUMN_THUMBNAIL_URL, video.getThumbnailURL());
            values.put(YouTubeVideoEntry.COLUMN_CHANNEL_TITLE, video.getChannelTitle());
            values.put(YouTubeVideoEntry.COLUMN_VIEWS_NUMBER, video.getViewCount());

            return db.insert(tableName, YouTubeVideoEntry.COLUMN_NAME_NULLABLE, values) > 0;


        }

        public boolean checkIfExists(String videoId) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String Query = "SELECT * FROM " + tableName + " WHERE " + YouTubeVideoEntry.COLUMN_VIDEO_ID + "='" + videoId + "'";
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
            return true;
        }

        public ArrayList<YouTubeVideo> readAll() {
            final String SELECT_QUERY_ORDER_DESC = "SELECT * FROM " + tableName + " ORDER BY "
                    + YouTubeVideoEntry.COLUMN_ENTRY_ID + " DESC";

            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ArrayList<YouTubeVideo> list = new ArrayList<>();

            Cursor c = db.rawQuery(SELECT_QUERY_ORDER_DESC, null);
            while (c.moveToNext()) {
                String videoId = c.getString(c.getColumnIndexOrThrow(YouTubeVideoEntry.COLUMN_VIDEO_ID));
                String title = c.getString(c.getColumnIndexOrThrow(YouTubeVideoEntry.COLUMN_TITLE));
                String duration = c.getString(c.getColumnIndexOrThrow(YouTubeVideoEntry.COLUMN_DURATION));
                String thumbnailUrl = c.getString(c.getColumnIndexOrThrow(YouTubeVideoEntry.COLUMN_THUMBNAIL_URL));
                String channelTitle = c.getString(c.getColumnIndexOrThrow(YouTubeVideoEntry.COLUMN_CHANNEL_TITLE));
                String viewsNumber = c.getString(c.getColumnIndexOrThrow(YouTubeVideoEntry.COLUMN_VIEWS_NUMBER));
                list.add(new YouTubeVideo(videoId, title, thumbnailUrl, duration, viewsNumber,channelTitle));
            }
            c.close();
            return list;
        }

        public boolean delete(String videoId) {
            return dbHelper.getWritableDatabase().delete(tableName,
                    YouTubeVideoEntry.COLUMN_VIDEO_ID + "='" + videoId + "'", null) > 0;
        }

        public boolean deleteAll() {
            return dbHelper.getWritableDatabase().delete(tableName, "1", null) > 0;
        }
    }



    private final class YouTubeDbHelper extends SQLiteOpenHelper {
        public YouTubeDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(YouTubeVideoEntry.DATABASE_FAVORITES_TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int i, int i1) {
            db.execSQL(YouTubeVideoEntry.DROP_QUERY_FAVORITES);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            super.onDowngrade(db, oldVersion, newVersion);
        }
    }





    /**
     * Inner class that defines Videos table entry
     */
    public static abstract class YouTubeVideoEntry implements BaseColumns {
        public static final String COLUMN_ENTRY_ID = "_id";
        public static final String COLUMN_VIDEO_ID = "video_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DURATION = "duration";
        public static final String COLUMN_THUMBNAIL_URL = "thumbnail_url";
        public static final String COLUMN_VIEWS_NUMBER = "views_number";
        public static final String COLUMN_CHANNEL_TITLE = "channel_title";

        public static final String COLUMN_NAME_NULLABLE = "null";

        private static final String DATABASE_FAVORITES_TABLE_CREATE =
                "CREATE TABLE " + FAVORITES_TABLE_NAME + "(" +
                        COLUMN_ENTRY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        COLUMN_VIDEO_ID + " TEXT NOT NULL UNIQUE," +
                        COLUMN_TITLE + " TEXT NOT NULL," +
                        COLUMN_DURATION + " TEXT," +
                        COLUMN_THUMBNAIL_URL + " TEXT," +
                        COLUMN_CHANNEL_TITLE + " TEXT," +
                        COLUMN_VIEWS_NUMBER + " TEXT)";

        public static final String DROP_QUERY_FAVORITES = "DROP TABLE " + FAVORITES_TABLE_NAME;
    }
}
